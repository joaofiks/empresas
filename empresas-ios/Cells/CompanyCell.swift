//
//  CompanyCell.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {
    
    /* *****************************************************************************
     **
     **  MARK: Variables declaration
     **
     *******************************************************************************/
    
    var card : UIView!
    
    var mainImage : UIImageView!
    
    var nameLabel : UILabel!
    
    /* ******************************************************************************
     **
     **  MARK: Init
     **
     ********************************************************************************/
    
    
    init(view: UIView) {
        super.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: nil)
        
        let width = view.frame.size.width
        let height = CGFloat(150)
        
        backgroundColor = .clear
        
      
        //--------------------------Card----------------------------
        
        card = UIView(frame: CGRect(x: width*0.05, y: 15, width: width*0.9, height: 135))
        card.backgroundColor = .generalPink()
        card.layer.cornerRadius = 7
        
        addSubview(card)
        
        
        //------------------------Main Image-------------------------
        
        mainImage = UIImageView(frame: CGRect(x: 0, y: 45, width: 35, height: 35))
        mainImage.contentMode = .scaleAspectFit
        mainImage.center.x = width/2
        
        addSubview(mainImage)
        
        //------------------------Name Label-------------------------
        
        nameLabel = UILabel(frame: CGRect(x: width*0.05, y: 85, width: width*0.9, height: 30))
        nameLabel.textColor = .white
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.appFont(size: 25, type: .bold)
        
        addSubview(nameLabel)
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
}
