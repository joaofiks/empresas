//
//  MainCoordinator.swift
//  empresas-ios
//
//  Created by João Vitor on 14/10/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator : Coordinator{
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController : UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = LoginVC.instantiate()
        vc.coordinator = self
        navigationController.isNavigationBarHidden = true
        navigationController.pushViewController(vc, animated: true)
    }
    
    func home() {
        let vc = HomeVC.instantiate()
        vc.coordinator = self
        navigationController.isNavigationBarHidden = true
        navigationController.pushViewController(vc, animated: true)
    }
    
    func companyDetails(enterprise : Enterprise, colour : UIColor) {
        let vc = CompanyDetailsVC.instantiate()
        vc.coordinator = self
        vc.enterprise = enterprise
        vc.colour = colour
        navigationController.isNavigationBarHidden = true
        navigationController.pushViewController(vc, animated: true)
    }
    
    func popViewController(){
        
        navigationController.popViewController(animated: true)
        
    }
    
}
