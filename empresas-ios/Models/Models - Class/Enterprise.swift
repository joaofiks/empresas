//
//  Enterprise.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit
import Alamofire

struct Enterprise{
    
    var id : Int
    
    var name : String
    
    var photo : String
    
    var city : String
    
    var country : String
    
    var description : String
    
}

class EnterpriseAPI{
    
    /* *********************************************************************************
    **
    **  MARK: Home Info
    **
    ***********************************************************************************/
    
    static func getEnterprise(search : String,callback: @escaping (ServerResponse) -> Void) {
        
        let newURL = API.host + API.enterprises + "?name=\(search)"
        
        let headers : HTTPHeaders = [
            "access-token": LoggedUser.sharedInstance.token,
            "client" : LoggedUser.sharedInstance.client,
            "uid" : LoggedUser.sharedInstance.uid
        ]
        
        print("resquest - getEnterprise")
        print(newURL)
        print(headers)
        
        Alamofire.request(newURL,
                          method: HTTPMethod.get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers: headers
            ).responseJSON(completionHandler: { response in
                
                let resp = ServerResponse()
                
                print("response - getEnterprise")
                print(response.result)
                
                switch response.result {
                    
                case let .success(value):
                    resp.statusCode = response.response?.statusCode ?? 0
                    
                    if response.response?.statusCode == 200 {
                        
                        let valueObject = value as AnyObject
                        
                        if let info = valueObject["enterprises"] as? [[String : Any]]{
                            
                            resp.enterprise = convertJSONInEnterpriseArray(info as AnyObject)
                            
                        }
                        
                        resp.success = true
                        
                        callback(resp)
                        
                        return
                        
                    }
                    
                case let .failure(error):
                    print(error)
                 
                }
                
                resp.success = false
                resp.errorMessage = "Erro carregando empresas"
                
                callback(resp)
                
            })
        
    }
    
    static func convertJSONInEnterpriseArray(_ JSON : AnyObject) -> [Enterprise] {
        
        var array : [Enterprise] = []
        
        guard let data = JSON as? NSArray else {
            return array
        }
        
        for item in data {
            
            array.append(convertJSONInEnterprise(item as AnyObject))
            
        }
        
        return array
        
    }
    
    static func convertJSONInEnterprise(_ JSON : AnyObject) -> Enterprise {
        
        var enterprise = Enterprise(id: -1, name: "", photo: "", city: "", country: "", description: "")
        
        if let info = JSON["id"] as? Int {
            enterprise.id = info
        }
        
        if let info = JSON["enterprise_name"] as? String {
            enterprise.name = info
        }
        
        if let info = JSON["description"] as? String {
            enterprise.description = info
        }
        
        if let info = JSON["photo"] as? String {
            enterprise.photo = info
        }
        
        if let info = JSON["city"] as? String {
            enterprise.city = info
        }
        
        if let info = JSON["country"] as? String {
            enterprise.country = info
        }
        
        return enterprise
        
    }
    
}
