//
//  Login.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit
import Alamofire

struct Login{
    
    var token : String
    
    var client : String
    
    var uid : String
    
}

class LoginAPI{
    
    /* **************************************************************************************************
     **
     **  MARK: signIn
     **
     ****************************************************************************************************/
    
    static func signIn(_ params : [String : Any], callback: @escaping (ServerResponse) -> Void) {
        
        let newURL = API.host + API.signin
        
        print ("request - signIn")
        print (newURL)
        print (params)
        
        Alamofire.request(newURL,
                          method : HTTPMethod.post,
                          parameters : params,
                          encoding : URLEncoding.default,
                          headers : nil
        ).responseJSON(completionHandler : { response in
            
            let resposta = ServerResponse()
            
            print ("Response - signIn")
            
            print (response.result.value)
            
            switch response.result {
                
            case let .success(value):
                resposta.statusCode = response.response?.statusCode ?? 0
                
                if response.response?.statusCode == 200 {
                    
                    var login = Login(token: "", client: "", uid: "")
                    
                    if let headers = response.response?.allHeaderFields as? [String : Any]{
                        
                        if let info = headers["access-token"] as? String {
                            login.token = info
                        }
                        
                        if let info = headers["client"] as? String {
                            login.client = info
                        }
                        
                        if let info = headers["uid"] as? String{
                            login.uid = info
                        }
                        
                    }
                    
                    LoggedUser.sharedInstance.token = login.token
                    
                    LoggedUser.sharedInstance.client = login.client
                    
                    LoggedUser.sharedInstance.uid = login.uid
                    
                    resposta.success = true
                    
                    callback (resposta)
                    
                    return
                    
                }
                
            case let .failure(error):
                print (error)
                
            }
            
            resposta.success = false
            resposta.errorMessage = "Não foi possível realizar login"
            
            callback (resposta)
            
        })
        
    }
    
}
