//
//  CompanyDetailsVC.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class CompanyDetailsVC: UIViewController, CompanyDetailsDelegate,Storyboarded {
    
    /* **************************************************************************************************
     **
     **  MARK: Variables Declaration
     **
     ****************************************************************************************************/
    
    weak var coordinator : MainCoordinator?
    
    var companyDetailsView : CompanyDetailsView!
    
    var enterprise : Enterprise!
    
    var presenter : CompanyDetailsPresenter!
    
    var colour : UIColor!
    
    /* **************************************************************************************************
     **
     **  MARK: View
     **
     ****************************************************************************************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companyDetailsView = CompanyDetailsView(view: view, parent: self)
        
        presenter = CompanyDetailsPresenter()
        
        presenter.delegate = self
        
        companyDetailsView.backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        presenter.showEnterprise(enterprise: enterprise)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /* **************************************************************************************************
    **
    **  MARK: Presenter Delegates
    **
    ****************************************************************************************************/
    
    func displayDetails(enterprise: Enterprise) {
        
        companyDetailsView.mainImage.image = UIImage(named: "companyImage")
        
        companyDetailsView.card.backgroundColor = colour
        
        companyDetailsView.nameLabel.text = enterprise.name
        
        companyDetailsView.companyName.text = enterprise.name
        
        companyDetailsView.detailsLabel.text = enterprise.description
        
        companyDetailsView.detailsLabel.frame.size.height = CGFloat(companyDetailsView.detailsLabel.maxNumberOfLines * 25)
        
        companyDetailsView.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: companyDetailsView.detailsLabel.frame.origin.y + companyDetailsView.detailsLabel.frame.height + 100)
        
    }
    
    /* **************************************************************************************************
     **
     **  MARK: Back Action
     **
     ****************************************************************************************************/
    
    @objc func backAction() {
        
        coordinator?.popViewController()
        
    }
    
}
