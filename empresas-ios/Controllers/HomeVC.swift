//
//  HomeVC.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, HomeDelegate,Storyboarded {
    
    /* **************************************************************************************************
     **
     **  MARK: Variables Declaration
     **
     ****************************************************************************************************/
    
    weak var coordinator : MainCoordinator?
    
    var homeView : HomeView!
    
    var presenter : HomePresenter!
    
    var colours : [UIColor] = [UIColor.lightBlue(), UIColor.salmon(), UIColor.greenFaded()]
    
    var currentIndex : Int = 0
    
    /* **************************************************************************************************
     **
     **  MARK: View
     **
     ****************************************************************************************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeView = HomeView(view: view, parent: self)
        
        presenter = HomePresenter()
        
        presenter.delegate = self
        
        homeView.tableView.delegate = self
        homeView.tableView.dataSource = self
        
        homeView.searchField.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /* **************************************************************************************************
    **
    **  MARK: Table View
    **
    ****************************************************************************************************/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.enterprises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = CompanyCell(view: view)
        
        cell.card.backgroundColor = colours[Int.random(in: 0...2)]
        
        cell.selectionStyle = .none
        
        cell.mainImage.image = UIImage(named: "companyImage")
        
        cell.nameLabel.text = presenter.enterprises[indexPath.row].name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let thisCell = homeView.tableView.cellForRow(at: indexPath) as! CompanyCell
        
        coordinator?.companyDetails(enterprise: self.presenter.enterprises[indexPath.row], colour: thisCell.card.backgroundColor!)
        
    }
    
    /* **************************************************************************************************
    **
    **  MARK: Presenter Delegates
    **
    ****************************************************************************************************/
    
    func displayResults() {
        
        UIView.animate(withDuration: 0.5){
            
            self.homeView.backgroundImage.frame.size.height = UIScreen.main.bounds.height*0.1
            
            self.homeView.searchField.center.y = self.homeView.backgroundImage.frame.origin.y + UIScreen.main.bounds.height*0.1
            
            self.homeView.foundLabel.frame.origin.y = self.homeView.searchField.frame.origin.y + self.homeView.searchField.frame.height + 15
            
            self.homeView.tableView.frame.origin.y = self.homeView.foundLabel.frame.origin.y + self.homeView.foundLabel.frame.height + 15
            
            self.homeView.tableView.frame.size.height = UIScreen.main.bounds.height - self.homeView.tableView.frame.origin.y
            
            self.homeView.backgroundImage.image = UIImage(named: "topBar")
            
        }
        
        self.homeView.tableView.reloadData()
        
    }
    
    func togglePlaceholder(message: String) {
        
        if message != ""{
            
            homeView.placeholderLabel.isHidden = false
            
            homeView.foundLabel.isHidden = !homeView.placeholderLabel.isHidden
            
        } else {
            
            homeView.placeholderLabel.isHidden = true
            
            homeView.foundLabel.isHidden = !homeView.placeholderLabel.isHidden
            
        }
        
        if !homeView.placeholderLabel.isHidden{
            
            homeView.placeholderLabel.text = message
            
        }
        
    }
    
    func showCount(count: Int) {
        
        homeView.foundLabel.text = "\(count) resultados encontrados"
        
        if count != 0{
            
            self.homeView.foundLabel.isHidden = false
            
        } else {
            
            self.homeView.foundLabel.isHidden = true
            
        }
        
    }
    
    /* **************************************************************************************************
    **
    **  MARK: Text Field Delegate
    **
    ****************************************************************************************************/
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.presenter.search(searchTerm: self.homeView.searchField.text!)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        homeView.searchField.resignFirstResponder()
        
        return true
        
    }
    
}
