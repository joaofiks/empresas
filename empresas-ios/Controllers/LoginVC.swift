//
//  LoginVC.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate, LoginDelegate,Storyboarded{
    
    /* **************************************************************************************************
     **
     **  MARK: Variables Declaration
     **
     ****************************************************************************************************/
    
    weak var coordinator : MainCoordinator?
    
    var loginView : LoginView!
    
    var presenter : LoginPresenter!
    
    /* **************************************************************************************************
     **
     **  MARK: View
     **
     ****************************************************************************************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginView = LoginView(view: view, parent: self)
        
        presenter = LoginPresenter()
        
        presenter.delegate = self
        
        loginView.enterButton.addTarget(self, action: #selector(enterAction), for: .touchUpInside)
        
        loginView.togglePassButton.addTarget(self, action: #selector(toggleSecureEntry), for: .touchUpInside)
        
        loginView.emailTextField.delegate = self
        
        loginView.passwordTextField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /* **************************************************************************************************
    **
    **  MARK: Presenter Delegates
    **
    ****************************************************************************************************/
    
    func showMessage(message: String) {
        
        self.loginView.wrongCredentialsLabel.isHidden = false
        
        self.loginView.wrongCredentialsLabel.text = message
        
        self.loginView.emailTextField.layer.borderColor = UIColor.wrongRed().cgColor
        self.loginView.emailTextField.layer.borderWidth = 1
        
        self.loginView.passwordTextField.layer.borderColor = UIColor.wrongRed().cgColor
        self.loginView.passwordTextField.layer.borderWidth = 1
        
        self.loginView.togglePassButton.setImage(UIImage(named: "wrong"), for: .normal)
        
        self.loginView.wrongEmailImage.isHidden = false
    }
    
    func showAlert(message: String) {
        
        let alert = UIAlertController(title: message, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func LoginSuccessful() {
        
        coordinator?.home()
        
    }
    
    func toggleLoaderVisibility() {
        
        self.toggleLoader(onView: view, backgroundColour: UIColor.black)
        
    }
    
    /* **************************************************************************************************
     **
     **  MARK: Enter Action
     **
     ****************************************************************************************************/
    
    @objc func enterAction() {
        
        loginView.emailTextField.resignFirstResponder()
        loginView.passwordTextField.resignFirstResponder()
        
        self.presenter.validateData(email: loginView.emailTextField.text!, password: loginView.passwordTextField.text!)
        
    }
    
    /* **************************************************************************************************
     **
     **  MARK: Text Field Delegate
     **
     ****************************************************************************************************/
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.loginView.wrongCredentialsLabel.isHidden = true
        
        self.loginView.emailTextField.layer.borderColor = UIColor.clear.cgColor
        self.loginView.emailTextField.layer.borderWidth = 1
        
        self.loginView.passwordTextField.layer.borderColor = UIColor.clear.cgColor
        self.loginView.passwordTextField.layer.borderWidth = 1
        
        self.loginView.togglePassButton.setImage(UIImage(named: "eye"), for: .normal)
        
        self.loginView.wrongEmailImage.isHidden = true
    }
    
    /* **************************************************************************************************
     **
     **  MARK: Toggle Secure Entry
     **
     ****************************************************************************************************/
    
    @objc func toggleSecureEntry() {
        
        loginView.passwordTextField.isSecureTextEntry.toggle()
        
    }
    
}
