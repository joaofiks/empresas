//
//  HomeView.swift
//  empresas-ios
//
//  Created by João Vitor on 21/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

class HomeView : UIView {
    
    
    /* ************************************************************************
     **
     **  MARK: Variables declaration
     **
     **************************************************************************/
    
    var scrollView: UIScrollView!
    
    var backgroundImage : UIImageView!
    
    var logo1 : UIImageView!
    
    var logo2 : UIImageView!
    
    var logo3 : UIImageView!
    
    var logo4 : UIImageView!
    
    var searchField : UITextField!
    
    var searchIcon : UIButton!
    
    var foundLabel : UILabel!
    
    var tableView : UITableView!
    
    var placeholderLabel : UILabel!
    
    /* ************************************************************************
     **
     **  MARK: Init
     **
     **************************************************************************/
    
    init(view: UIView, parent: UIViewController) {
        super.init(frame: view.frame);
        
        let width = view.frame.size.width
        let height = view.frame.size.height
        
        view.backgroundColor = UIColor.white
        
        var yPosition = CGFloat (height*0.1)
        
        //------------------------- Scroll View -----------------------------
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        scrollView.isScrollEnabled = false
        scrollView.backgroundColor = .clear
        
        view.addSubview(scrollView)
        
        //------------------------Background Image---------------------------
        
        backgroundImage = UIImageView(frame: CGRect(x: 0, y: -height*0.05, width: width, height: height*0.25))
        backgroundImage.image = UIImage(named: "homeBackground")
        backgroundImage.center.x = width/2
        backgroundImage.contentMode = .scaleToFill
        
        scrollView.addSubview(backgroundImage)
        
        //--------------------------Search Field-----------------------------
        
        searchField = UITextField(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 60))
        searchField.layer.cornerRadius = 5
        searchField.center.y = height*0.2
        searchField.backgroundColor = UIColor.textFieldBackground()
        searchField.keyboardType = .emailAddress
        searchField.autocorrectionType = .no
        searchField.autocapitalizationType = .none
        searchField.placeholder = "Busque por empresa"
        searchField.font = UIFont.appFont(size: 20, type: .regular)
        searchField.tintColor = UIColor.generalPink()
        
        scrollView.addSubview(searchField)
        
        searchIcon = UIButton(type: .custom)
        searchIcon.setImage(UIImage(named: "search"), for: .normal)
        searchIcon.imageEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        searchIcon.frame = CGRect(x: CGFloat(searchField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        searchField.leftView = searchIcon
        searchField.leftViewMode = .always
        
        yPosition = searchField.frame.origin.y + searchField.frame.height + 10
        
        //--------------------------Found Label---------------------------------
        
        foundLabel = UILabel(frame: CGRect(x: width*0.05, y: yPosition, width: width*0.9, height: 30))
        foundLabel.textColor = UIColor.greyText()
        foundLabel.font = UIFont.appFont(size: 14, type: .regular)
        foundLabel.isHidden = true
        
        scrollView.addSubview(foundLabel)
        
        yPosition = yPosition + foundLabel.frame.height + 10
        
        //------------------------- Table View ---------------------------------
        
        tableView = UITableView(frame: CGRect(x: 0, y: yPosition, width: width, height: height - yPosition))
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = true
        tableView.backgroundColor = .clear
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: width*0.1, right: 0)
        
        scrollView.addSubview(tableView)
        
        //------------------------- Placeholder Label ---------------------------------
        
        placeholderLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: 40))
        placeholderLabel.textColor = .greyText()
        placeholderLabel.font = UIFont.appFont(size: 22, type: .regular)
        placeholderLabel.textAlignment = .center
        placeholderLabel.isHidden = true
        placeholderLabel.center.y = height*0.4
        
        scrollView.addSubview(placeholderLabel)
        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
