//
//  CompanyDetailsPresenter.swift
//  empresas-ios
//
//  Created by João Vitor on 13/10/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation

protocol CompanyDetailsDelegate {
    
    func displayDetails(enterprise : Enterprise)

}

class CompanyDetailsPresenter : NSObject{
    
    /* ************************************************************************
    **
    **  MARK: Variables declaration
    **
    **************************************************************************/
    
    var delegate : CompanyDetailsDelegate!
    
    /* ************************************************************************
    **
    **  MARK: Search
    **
    **************************************************************************/
    
    func showEnterprise(enterprise : Enterprise){
        
        self.delegate.displayDetails(enterprise: enterprise)
        
    }
    
}

