//
//  HomePresenter.swift
//  empresas-ios
//
//  Created by João Vitor on 13/10/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation

protocol HomeDelegate {
    
    func displayResults()
    func togglePlaceholder(message : String)
    func showCount(count : Int)

}

class HomePresenter : NSObject{
    
    /* ************************************************************************
    **
    **  MARK: Variables declaration
    **
    **************************************************************************/
    
    var delegate : HomeDelegate!
    
    var enterprises : [Enterprise] = []
    
    /* ************************************************************************
    **
    **  MARK: Search
    **
    **************************************************************************/
    
    func search(searchTerm : String){
        
        EnterpriseAPI.getEnterprise(search: searchTerm){(response) in
            
            if response.success{
                
                self.enterprises = response.enterprise
                
                if self.enterprises.count == 0{
                    
                    self.delegate.togglePlaceholder(message: "Nenhum resultado encontrado")
                    
                } else {
                    
                    self.delegate.togglePlaceholder(message: "")
                    
                }
                
                self.delegate.displayResults()
                
                self.delegate.showCount(count: self.enterprises.count)
                
            }
            
        }
        
    }
    
}
