//
//  LoginPresenter.swift
//  empresas-ios
//
//  Created by João Vitor on 13/10/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation

protocol LoginDelegate {
    func showMessage(message : String)
    func showAlert(message : String)
    func toggleLoaderVisibility()
    func LoginSuccessful()
}

class LoginPresenter : NSObject{
    
    /* ************************************************************************
    **
    **  MARK: Variables declaration
    **
    **************************************************************************/
    
    var delegate : LoginDelegate!
    
    /* ************************************************************************
    **
    **  MARK: Validate Data
    **
    **************************************************************************/
    
    func validateData (email : String, password : String){
        
        if email.count != 0 {
            
            if password.count != 0{
                
                login(email: email, password: password)
                
            } else {
                
                self.delegate.showAlert(message: "O campo senha deve ser preenchido!")
                
            }
            
        } else {
            
            self.delegate.showAlert(message: "O campo email deve ser preenchido!")
            
        }
        
    }
    
    /* ************************************************************************
    **
    **  MARK: login
    **
    **************************************************************************/
    
    fileprivate func login(email : String, password : String){
        
        self.delegate.toggleLoaderVisibility()
        
        let params = ["email" : email,
                      "password" : password]
        
        LoginAPI.signIn(params){(response) in
            
            self.delegate.toggleLoaderVisibility()
            
            if response.success{
                
                self.delegate.LoginSuccessful()
                
            } else {
                
                self.delegate.showMessage(message: "Credenciais Incorretas")
                
            }
            
        }
        
    }
    
}
