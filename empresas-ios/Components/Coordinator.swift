//
//  Coordinator.swift
//  empresas-ios
//
//  Created by João Vitor on 14/10/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
    
}
