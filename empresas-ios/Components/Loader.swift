//
//  Loader.swift
//  empresas-ios
//
//  Created by João Vitor on 22/09/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import UIKit

var loader : UIView?

var loadImage : UIImageView!

var loadImage2 : UIImageView!

var isLoaderShown : Bool = false
 
extension UIViewController {
    func showLoad(onView : UIView, backgroundColour : UIColor) {
        
        let loadView = UIView.init(frame: onView.bounds)
        
        loadView.backgroundColor = backgroundColour
        
        loadView.alpha = 0.8
        
        loadImage = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.width*0.4, height: view.frame.width*0.4))
        loadImage.image = UIImage(named: "load")
        loadImage.center = loadView.center
        
        loadImage2 = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.width*0.3, height: view.frame.width*0.3))
        loadImage2.image = UIImage(named: "load")
        loadImage2.center = loadView.center
        
        DispatchQueue.main.async {
            onView.addSubview(loadView)
            onView.addSubview(loadImage)
            onView.addSubview(loadImage2)
        }
        
        loader = loadView
        loadImage.rotate()
        loadImage2.counterRotate()
    }
    
    func removeLoad() {
        DispatchQueue.main.async{
            loadImage.removeFromSuperview()
            loadImage2.removeFromSuperview()
            loader?.removeFromSuperview()
            loader = nil
        }
    }
    
    func toggleLoader(onView : UIView, backgroundColour : UIColor){
        
        isLoaderShown.toggle()
        
        if isLoaderShown{
            showLoad(onView: onView, backgroundColour: backgroundColour)
        } else {
            removeLoad()
        }
        
    }
    
}

extension UIImageView{
    
    func rotate() {
        
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 2
        rotation.isCumulative = true
        rotation.repeatCount = Float.infinity
        self.layer.add(rotation, forKey: "rotationAnimation")
        
    }
    
    func counterRotate() {
        
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: -Double.pi * 2)
        rotation.duration = 2
        rotation.isCumulative = true
        rotation.repeatCount = Float.infinity
        self.layer.add(rotation, forKey: "rotationAnimation")
        
    }
    
}
