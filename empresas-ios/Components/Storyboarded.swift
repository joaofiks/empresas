//
//  Storyboarded.swift
//  empresas-ios
//
//  Created by João Vitor on 14/10/20.
//  Copyright © 2020 joaoVitorFiks. All rights reserved.
//

import Foundation
import UIKit

protocol Storyboarded {
    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController{
    
    static func instantiate() -> Self{
        
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(identifier: id) as! Self
        
    }
    
}
